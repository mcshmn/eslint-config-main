module.exports = {
	env: {
		'browser': true,
		'commonjs': true,
		'es6': true,
		'node': true
	},
	extends: 'eslint:recommended',
	parserOptions: {
		'ecmaVersion': 2018
	},
	rules: {
		'indent': [
			'error',
			'tab',
		],
		'linebreak-style': [
			'error',
			'unix',
		],
		'quotes': [
			'error',
			'single',
		],
		'semi': [
			'error',
			'never',
		],
		'array-bracket-newline': [
			'error',
			{
				'multiline': true,
				'minItems': 2,
			},
		],
		'array-element-newline': [
			'error',
			{
				'multiline': true,
				'minItems': 2,
			},
		],
		'array-bracket-spacing': [
			'error',
			'never',
		],
		'block-spacing': [
			'error',
			'never',
		],
		'brace-style': 'error',
		'camelcase': 'error',
		'comma-dangle': [
			'error',
			'always-multiline',
		],
		'comma-spacing': [
			'error',
			{
				'before': false,
				'after': true,
			},
		],
		'comma-style': [
			'error',
			'last',
		],
		'computed-property-spacing': [
			'error',
			'never',
		],
		'func-call-spacing': [
			'error',
			'never',
		],
		'func-name-matching': 'error',
		'func-names': [
			'error',
			'always',
		],
		'implicit-arrow-linebreak': [
			'error',
			'beside',
		],
		'key-spacing': [
			'error',
			{'beforeColon': false},
		],
		'keyword-spacing': [
			'error',
			{'before': true},
		],
		'new-parens': 'error',
		'no-lonely-if': 'error',
		'no-trailing-spaces': 'error',
		'no-whitespace-before-property': 'error',
		'no-unneeded-ternary': 'error',
		'object-curly-newline': [
			'error',
			{'multiline': true},
		],
		'object-curly-spacing': [
			'error',
			'never',
		],
		'object-property-newline': [
			'error',
			{'allowAllPropertiesOnSameLine': false},
		],
		'operator-linebreak': [
			'error',
			'none',
		],
		'prefer-object-spread': 'error',
		'space-unary-ops': 'error',
		'arrow-body-style': [
			'error',
			'as-needed',
		],
		'arrow-parens': [
			'error',
			'as-needed',
		],
		'arrow-spacing': 'error',
		'no-useless-computed-key': 'error',
		'no-var': 'error',
		'prefer-const': 'error',
		'prefer-destructuring': [
			'error',
			{
				'VariableDeclarator': {
					'object': true,
					'array': true,
				},
			},
		],
		'prefer-spread': 'error',
		'prefer-template': 'error',
		'rest-spread-spacing': [
			'error',
			'never',
		],
		'template-curly-spacing': 'error',
		'no-console': process.env.NODE_ENV === 'production' ? 'error' : 'off',
		'no-debugger': process.env.NODE_ENV === 'production' ? 'error' : 'off',
	}
}